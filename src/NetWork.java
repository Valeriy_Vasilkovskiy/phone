import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NetWork implements INetWork {

    NetWork() {
//        if(name_operator.equals("vodafone")){
//            ArrayList<String> vodafone = new ArrayList<>();
//        }
//        if(name_operator.equals("kyivstar")){
//            ArrayList<String> kyivstar = new ArrayList<>();
//        }
    }

    private ArrayList<String> vodafone = new ArrayList<>();

    private ArrayList<String> kyivstar = new ArrayList<>();

//    private ArrayList<String> life = new ArrayList<>();

    private ArrayList<String> noneOperator = new ArrayList<>();

    @Override
    public String call(String number) {
        boolean isContein = false;
        String answer;

        for (String a : cheekOperator(number)) {
            if (a != null && a.equals(number)) {
                isContein = true;
                break;
            }
        }
        if (isContein) {
            answer = "Звоню";
        } else {
            answer = "Данного номера не существует";
        }
        return answer;
    }

    @Override
    public String addToBase(String number) {
        ArrayList<String> buf = cheekOperator(number);
        if (buf == noneOperator) {
            return "У данного номера нет оператора";
        }
        String answer;
        if (cheekNumber(number) && equalNumber(buf, number)) {
            buf.add(number);
            answer = "Удачно сохранён";
        } else {
            answer = "Неправильный ввод или же номер уже существует в базе";
        }
        return answer;
    }

    private boolean cheekNumber(String number) {
        Pattern pattern = Pattern.compile("^\\+380[0-9]{9}");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }

    private boolean equalNumber(ArrayList<String> operator, String number) {
        boolean info = true;
        for (String s : operator) {
            if (s != null) {
                if (!s.equals(number)) {
                    info = false;
                }
            }
        }
        return info;
    }

    private ArrayList<String> cheekOperator(String string) {
        if (string == null) {
            return noneOperator;
        }
        char[] info = string.toCharArray();
        String infoAboutOp = "";
        for (int i = 4; i < 6; i++) {
            infoAboutOp = infoAboutOp.concat(String.valueOf(info[i]));
        }
        switch (infoAboutOp) {
            case "68":
                return kyivstar;

            case "95":
                return vodafone;
        }
        return noneOperator;
    }
}
